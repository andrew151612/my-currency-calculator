import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store( {
    state: {
        // создаем пустой массив, в который положим данные о валютах
        valute: []
    },
    actions: {
        // получаем данные о валютах по курсу ЦБ РФ в формате JSON
        GET_VALUTE_FROM_API({commit}) {
            return axios('https://www.cbr-xml-daily.ru/daily_json.js', {
                method: 'GET'
            })
            .then((response) => {
                commit('SET_VALUTES_TO_VUEX', response.data)
            })
        }
    },
    mutations: {
        // изменяем состояние state: кладем полученные данные в формате JSON в массив valute в state
        SET_VALUTES_TO_VUEX: (state, valutes) => {
            state.valute = valutes.Valute
        }
    },
    getters: {
        // получаем состояние state
        VALUTES(state) {
            return state.valute 
        }

    }
})
export default store;